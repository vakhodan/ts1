package org.example;

public class factorial {
    public static void main(String[] args) {
        System.out.println("Factorial of 5 (R): "+factorialRecursive(5));
    }

    public static int factorialRecursive(int n){
        if(n <= 1){
            return 1;
        }
        else{
            return n*factorialRecursive(n-1);
        }
    }
}
