package org.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class factorialTest {

    @BeforeEach
    void setUp() {
        factorial factorial1 = new factorial();
    }

    @Test
    void factorialRecursive() {
        int expected = 2;
        int actual = factorial.factorialRecursive(2);
        assertEquals(expected, actual);
    }
}