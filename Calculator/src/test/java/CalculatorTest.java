import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    Calculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new Calculator();
    }

    @Test

    void add() {
        int input1 = 8;
        int input2 = 2;
        int expected = 10;
        int actual = calculator.add(input1, input2);
        assertEquals(expected, actual);
    }

    @Test
    void mult() {
        int input1 = 8;
        int input2 = 2;
        int expected = 16;
        int actual = calculator.mult(input1, input2);
        assertEquals(expected, actual);
    }

    @Test
    void divEightByTwoEqualsFour() {
        int input1 = 8;
        int input2 = 2;
        int expected = 4;
        int actual = calculator.div(input1, input2);
        assertEquals(expected, actual);
    }
    @Test
    void divEightByZeroException() {
        int input1 = 8;
        int input2 = 0;
        assertThrows(ArithmeticException.class, () -> calculator.div(input1, input2));
    }

    @Test
    void sub() {
        int input1 = 8;
        int input2 = 2;
        int expected = 6;
        int actual = calculator.sub(input1, input2);
        assertEquals(expected, actual);
    }
}