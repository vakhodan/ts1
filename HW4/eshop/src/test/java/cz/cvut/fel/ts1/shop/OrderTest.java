package cz.cvut.fel.ts1.shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


class OrderTest {
    @Test
    public void testConstructorWithState() {
        String customerName = "TestName";
        String customerAddress = "TestAddress";
        int state = 1;
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "TestItem", 1F, "TestCategory", 10));
        Order order = new Order(cart, customerName, customerAddress, state);

        Assertions.assertNotNull(order);
        Assertions.assertEquals(cart.getCartItems(), order.getItems());
        Assertions.assertEquals(customerName, order.getCustomerName());
        Assertions.assertEquals(customerAddress, order.getCustomerAddress());
        Assertions.assertEquals(state, order.getState());
    }

    @Test
    public void testConstructorWithNoState() {
        int expectedState = 0;
        String customerName = "TestName";
        String customerAddress = "TestAddress";
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "TestItem", 1F, "TestCategory", 10));
        Order order = new Order(cart, customerName, customerAddress);

        Assertions.assertNotNull(order);
        Assertions.assertEquals(cart.getCartItems(), order.getItems());
        Assertions.assertEquals(customerName, order.getCustomerName());
        Assertions.assertEquals(customerAddress, order.getCustomerAddress());
        Assertions.assertEquals(expectedState, order.getState());
    }
}