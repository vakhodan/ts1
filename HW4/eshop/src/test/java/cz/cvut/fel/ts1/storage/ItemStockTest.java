package cz.cvut.fel.ts1.storage;

import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class ItemStockTest {
    static ItemStock itemStock;
    static StandardItem testItem;

    @BeforeEach
    void setUp() {
        testItem = new StandardItem(1, "testItem", 10f, "TestCategory", 11);
        itemStock = new ItemStock(testItem);
    }
    @AfterEach
    void tearDown() {
        itemStock = null;
        testItem = null;
    }
    @Test
    public void constructorTest() {


        assertNotNull(itemStock);
        assertEquals(testItem, itemStock.getItem());
        assertEquals(0, itemStock.getCount());
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 5, 10})
    public void increaseCountTest(int count) {
        itemStock.IncreaseItemCount(count);
        assertEquals(count, itemStock.getCount());
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 5, 10})
    public void decreaseCountTest(int count) {
        itemStock.IncreaseItemCount(15);
        itemStock.decreaseItemCount(count);
        assertEquals(15 - count, itemStock.getCount());
    }

}