package cz.cvut.fel.ts1.shop;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class StandartItemTest {
    @Test
    public void testConstructor() {
        StandardItem item = new StandardItem(1, "TestItem", 10.0f, "TestCategory", 5);
        assertNotNull(item);
        assertEquals(1, item.getID());
        assertEquals("TestItem", item.getName());
        assertEquals(10.0f, item.getPrice());
        assertEquals("TestCategory", item.getCategory());
        assertEquals(5, item.getLoyaltyPoints());
    }

    @Test
    public void testCopy() {
        StandardItem original = new StandardItem(1, "OriginalItem", 20.0f, "OriginalCategory", 10);
        StandardItem copy = original.copy();
        assertNotNull(copy);
        assertEquals(original.getID(), copy.getID());
        assertEquals(original.getName(), copy.getName());
        assertEquals(original.getPrice(), copy.getPrice());
        assertEquals(original.getCategory(), copy.getCategory());
        assertEquals(original.getLoyaltyPoints(), copy.getLoyaltyPoints());
    }

    @ParameterizedTest
    @CsvSource({
            "1, TestItem, 10.0, TestCategory, 5, true",
            "2, AnotherItem, 15.0, AnotherCategory, 8, true",
            "3, TestItem, 10.0, TestCategory, 3, true",
            "4, TestItem, 12.0, TestCategory, 5, true",
            "5, TestItem, 10.0, AnotherCategory, 5, true",
            "6, TestItem, 10.0, TestCategory, 5, true"
    })
    public void testEquals(int id1, String name1, float price1, String category1, int loyaltyPoints1, boolean expectedResult) {
        StandardItem item1 = new StandardItem(id1, name1, price1, category1, loyaltyPoints1);
        StandardItem item2 = new StandardItem(id1, name1, price1, category1, loyaltyPoints1);
        assertEquals(expectedResult, item1.equals(item2));
    }
}
