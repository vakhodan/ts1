package cz.cvut.fel.ts1.archive;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.Order;
import org.junit.jupiter.api.Test;


import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


public class PurchasesArchiveTest {
    @Test
    public void testPrintItemPurchaseStatistics() {
        ItemPurchaseArchiveEntry mockEntry = mock(ItemPurchaseArchiveEntry.class);
        when(mockEntry.toString()).thenReturn("Test data");
        HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive = new HashMap<>();
        itemArchive.put(1, mockEntry);

        Order mockOrder = mock(Order.class);

        ArrayList<Order> orderArchive = new ArrayList<>();

        orderArchive.add(mockOrder);

        PurchasesArchive archive = new PurchasesArchive(itemArchive, orderArchive);

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        archive.printItemPurchaseStatistics();

        assertEquals("ITEM PURCHASE STATISTICS:\nTest data\n", outContent.toString());
        System.setOut(System.out);
    }

    @Test
    public void testgetHowManyTimesHasBeenItemSold() {
        ItemPurchaseArchiveEntry mockEntry = mock(ItemPurchaseArchiveEntry.class);
        when(mockEntry.getCountHowManyTimesHasBeenSold()).thenReturn(5);
        HashMap<Integer, ItemPurchaseArchiveEntry> itemPurchaseArchive = new HashMap<>();
        ArrayList<Order> orderArchive = new ArrayList<>();
        itemPurchaseArchive.put(1, mockEntry);
        Item mockItem = mock(Item.class);
        when(mockItem.getID()).thenReturn(1);
        PurchasesArchive archive = new PurchasesArchive(itemPurchaseArchive, orderArchive);
        int output = archive.getHowManyTimesHasBeenItemSold(mockItem);
        assertEquals(5, output);
    }

    @Test
    public void testPutOrderToPurchasesArchive() {
        Order mockOrder = mock(Order.class);
        Item mockItem = mock(Item.class);
        ArrayList<Item> itemList = new ArrayList<>();
        itemList.add(mockItem);

        when(mockOrder.getItems()).thenReturn(itemList);
        when(mockItem.getID()).thenReturn(10);

        PurchasesArchive archive = new PurchasesArchive();

        archive.putOrderToPurchasesArchive(mockOrder);

        assertEquals(1, archive.getHowManyTimesHasBeenItemSold(mockItem));
        verify(mockOrder).getItems();
    }
    @Test
    public void testConstructorItemPurchaseArchiveEntry() {
        Item mockItem = mock(Item.class);
        ItemPurchaseArchiveEntry itemPurchaseArchiveEntry = new ItemPurchaseArchiveEntry(mockItem);

        assertNotNull(itemPurchaseArchiveEntry);
        assertEquals(mockItem, itemPurchaseArchiveEntry.getRefItem());
    }


}
